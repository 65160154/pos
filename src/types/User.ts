type Gender = 'male' | 'female' | 'others'
type Role = 'admin' | 'user'
type User = {
    id: number
    email: string
    password: string
    fullname: string
    gender: Gender // Male,Female,Other
    roles: Role[] //admin,user
}

export type {Gender, Role, User}
